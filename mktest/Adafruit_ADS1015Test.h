#include <iostream>
#include <unistd.h>

#include "gtest/gtest.h"
#include "Adafruit_ADS1015.h"

#define FETCH_NUM 10
#define AD_CHANNEL 0

using namespace std;

class Adafruit_ADS1015Test: public ::testing::Test{
    virtual void SetUp(){
    };

    protected :
        Adafruit_ADS1015 a2d;
};


TEST_F(Adafruit_ADS1015Test, FetchValue){
    uint16_t value;

    for(int i = 0; i < FETCH_NUM; i++){
        value = a2d.readADC_SingleEnded(AD_CHANNEL);  

        //For ADS1015 at max range (+/-6.144V) 1 bit = 3mV (12-bit values)
        cout << "Channel " << AD_CHANNEL <<  " = ";
        cout << static_cast<unsigned int>(value)*0.003 << " Volts" << endl;
        usleep(100000);
    }
};

