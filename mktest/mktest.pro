TARGET           = test_tsensor
TEMPLATE         = app
CONFIG          -= qt   #exclude the linkage to -lQtGUI and -lQtCore

INCLUDEPATH     += ../include ./

OBJECTS_DIR     += .obj

LIBS            += -lpthread -lgtest -L"../lib/" -ltsensor 

QMAKE_CXXFLAGS  +=-g -std=c++0x

HEADERS         +=  ./Adafruit_ADS1015Test.h\
                    ./FSRSensorTest.h

SOURCES         +=  test.cpp

