#include <iostream>
#include <unistd.h>

#include "FSRSensor.h"
#include "gtest/gtest.h"

#define FETCH_NUM 1000
#define AD_CHANNEL 0

using namespace std;

class FSRSensorTest: public ::testing::Test{
    virtual void SetUp(){
    };

    protected :
        FSRSensor sensor;
};


TEST_F(FSRSensorTest, DISABLED_FetchRawValue){
    
    for(int i = 0; i < FETCH_NUM; i++){
        cout << "Channel " << AD_CHANNEL <<  " ; getRawFroce = ";
        cout << sensor.getRawForce(AD_CHANNEL) << " grams"<< endl;
        usleep(100000);
    }
};


TEST_F(FSRSensorTest, FetchCalibValue){

    sensor.calibFSR(AD_CHANNEL);

    for(int i = 0; i < FETCH_NUM; i++){
        cout << "Channel " << AD_CHANNEL <<  " ; getForce = ";
        cout << sensor.getForce(AD_CHANNEL) << " grams"<< endl;
        usleep(100000);
    }
};

