/**
 * \file FSRSensor.cpp
 * \class FSRSensor
 * 
 * \brief Implementation of Class FSRSensor
 *
 * \author Milk Phongtharin; pv-milk@cvl.iis.u-tokyo.ac.jp
 */
#include <iostream>
#include <assert.h>

#include "Adafruit_ADS1015.h"
#include "FSRSensor.h"

const unsigned int  FSRSensor::CHANNEL_NUM              = 4;
const unsigned int  FSRSensor::DEFAULT_UNPRESSED_VALUE  = 1111;       /* BEFORE multiply conversion value */
const double        FSRSensor::VOLTAGE_CONVERSION       = 0.003;

FSRSensor::FSRSensor(unsigned int sensor_gain, unsigned int pulldown)
    :gain(sensor_gain), pulldown_resistor(pulldown)
{
    a2d         = new Adafruit_ADS1015();
    calib_value = new unsigned int[CHANNEL_NUM];
    for(unsigned int i = 0; i < CHANNEL_NUM; i++){
        calib_value[i] = DEFAULT_UNPRESSED_VALUE;
    }
};

FSRSensor::~FSRSensor(){
    delete a2d;
    delete calib_value;
};

FSRSensor::FSRSensor(const FSRSensor& fsr){
    (void)fsr;      /// Get rid of compiler's unused warning

};

FSRSensor& FSRSensor::operator=(const FSRSensor& fsr){
    (void)fsr;      /// Get rid of compiler's unused warning
    return *this;
};

unsigned int FSRSensor::getRawValue(unsigned int channel){
    assert(channel < CHANNEL_NUM);
    return (unsigned int) a2d->readADC_SingleEnded(channel);
};

double FSRSensor::voltage2force(double voltage){
    double MAX_VOLTAGE = VOLTAGE_CONVERSION*DEFAULT_UNPRESSED_VALUE;
    //std::cout << "max : "<<  MAX_VOLTAGE << std::endl;
    return gain*(MAX_VOLTAGE - voltage)/(pulldown_resistor*voltage);
};

double FSRSensor::getRawVoltage(unsigned int channel){
    if(channel < CHANNEL_NUM){
        return VOLTAGE_CONVERSION*getRawValue(channel);
    }else{
        return -1;
    }
};

double FSRSensor::getRawForce(unsigned int channel){
    if(channel < CHANNEL_NUM){
        return voltage2force(getRawVoltage(channel));
    }else{
        return -1;
    }
};

void FSRSensor::calibFSR(unsigned int channel){
    if(channel < CHANNEL_NUM){
        calib_value[channel] = getRawValue(channel);
    }
};

double FSRSensor::getForce(unsigned int channel){
    if(channel < CHANNEL_NUM){
        double base_force       = voltage2force(calib_value[channel]*VOLTAGE_CONVERSION);
        //std::cout << "base_force : " << base_force << std::endl;
        double current_force    = getRawForce(channel) - base_force;
        return (current_force > 0)? current_force :0;
    }else{
        return -1;
    }
};

//EOF
