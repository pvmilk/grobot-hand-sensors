/**
 * \file  FSRSensor.h
 * \class FSRSensor
 * 
 * \brief Header of Class FSRSensor (Force Sensing Resistor sensor)
 *
 * \author Milk Phongtharin; pv-milk@cvl.iis.u-tokyo.ac.jp
 */

#ifndef _FSRSensor_H_
#define _FSRSensor_H_

#define INABA_SRD_H_GAIN    1160
#define PULLDOWN_RESISTOR   20      /* in Kilo-OHM */

class Adafruit_ADS1015;

/*!
 *  Class FSRSensor
 *
 */
class FSRSensor{
    private:
        static const unsigned int CHANNEL_NUM;
        static const unsigned int DEFAULT_UNPRESSED_VALUE;
        static const double       VOLTAGE_CONVERSION;

    ///////////////////////////// METHOD ////////////////////////////////
    public:
        /// Constructor 
        FSRSensor(unsigned int sensor_gain = INABA_SRD_H_GAIN, unsigned int pulldown = PULLDOWN_RESISTOR);

        /// Destructor
        ~FSRSensor();

    private:
        /// Copy Constructor    --> Disable
        FSRSensor(const FSRSensor& fsr);

        /// Operator=           --> Disable
        FSRSensor& operator=(const FSRSensor& fsr);

        /// Get the raw value captured from a2d ADS1015
        unsigned int getRawValue(unsigned int channel);

        /// Internal method to convert from voltage to force
        double voltage2force(double voltage);

    public:
        /// Get the raw voltage (in voltage) captured from a2d ADS1015
        double getRawVoltage(unsigned int channel);

        /// Get the raw estimated force (in grams) applied on the sensor.
        double getRawForce(unsigned int channel);

        /// Calibrate the sensor;
        /// In other word, the current situation when this method is called will be considered as 0 grams.
        void calibFSR(unsigned int channel);

        /// Get the estimated force (in grams) applied on the sensor;
        /// The raw force is get deducted from the base force, which is capture during calibFSR is called.
        double getForce(unsigned int channel);

    ///////////////////////////// VARIABLES /////////////////////////////
    private:
        Adafruit_ADS1015    *a2d;
        unsigned int        *calib_value;
        unsigned int        gain;
        unsigned int        pulldown_resistor;
};

#endif//_FSRSensor_H_
//EOF
