TARGET = tsensor
TEMPLATE = lib

CONFIG += staticlib object_with_source

CONFIG -= qt

DESTDIR = ../lib/

INCLUDEPATH += ../include

DEPENDPATH += ../src ../include

OBJECTS_DIR += .obj

QMAKE_CXXFLAGS  +=-g -std=c++0x

HEADERS +=  ../include/Adafruit_ADS1015.h\
            ../include/FSRSensor.h

SOURCES +=  ../src/Adafruit_ADS1015.cpp\
            ../src/FSRSensor.cpp



